import 'package:flutter/material.dart';
import 'package:movie_info_app/constants.dart';

import 'genres.dart';
import 'question.dart';
import 'movie_carousel.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // mengaktifkan gulir pada perangkat kecil
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Question(),
          Genres(),
          SizedBox(height: kDefaultPadding),
          MovieCarousel(),
        ],
      ),
    );
  }
}
