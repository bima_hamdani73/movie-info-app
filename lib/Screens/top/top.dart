import 'package:flutter/material.dart';

class Top extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Container(
        margin: EdgeInsets.only(left: 0),
        width: 300,
        alignment: Alignment.topLeft,
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Text(
          "Top Movie of the Year ",
          style: TextStyle(
              fontWeight: FontWeight.bold, color: Colors.black, fontSize: 30),
          textAlign: TextAlign.left,
        ),
      ),
    );
  }
}
